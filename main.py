#!/usr/bin/env python3
from pyreddit.app import Pyreddit
import py_cui

app = Pyreddit()
def focus_categories():
        app.ui_root.move_focus(app.ui.categories_list)
app.ui_root.add_key_command(py_cui.keys.KEY_C_LOWER, focus_categories)
app.main()
