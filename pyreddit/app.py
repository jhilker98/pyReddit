#!/usr/bin/env python3
import py_cui
from pyreddit.ui import PyredditUI
from pyreddit.parser import FeedParser, ConfigParser
from bs4 import BeautifulSoup
import textwrap
import datetime
import os
import webbrowser

class Pyreddit:
        
        feed_parser = FeedParser(os.path.join(os.path.expanduser('~'), ".config", "pyreddit", "feeds.yaml"))
        config_parser = ConfigParser(os.path.join(os.path.expanduser("~"), ".config", "pyreddit", "config.yaml"))
        
        #feed_parser = FeedParser("feeds.yaml")
        #config_parser = ConfigParser("config.yaml")
        ui_root = py_cui.PyCUI(7,7)
        ui = PyredditUI(ui_root)
        active_category = None
        feed_title = None
        feed_entries = None
        soup = None
        config = None

        def __init__(self):
            #self.config = self.config_parser.
            self.ui_root.set_title("Newsfeed")
            self.config = self.config_parser.get_config_file()            
            self.ui.categories_list.add_item_list(self.feed_parser.get_feed_categories())            
            self.ui.categories_list.add_key_command(py_cui.keys.KEY_ENTER, self.print_active_subs)
            self.ui.sub_list.add_key_command(py_cui.keys.KEY_ENTER, self.print_feed_entries)
            
            self.ui.post_list.add_key_command(py_cui.keys.KEY_ENTER, self.print_feed_post)
            
            #self.ui_root.add_key_command(py_cui.keys.KEY_C_LOWER, self.ui.focus_categories())
            
            #self.ui_root.add_key_command(py_cui.keys.KEY_S_LOWER, self.ui.focus_subreddits())
            
            self.ui.post_list.add_key_command(py_cui.keys.KEY_O_LOWER, self.open_in_browser)
            if self.config['unicode'] == True:
                self.ui_root.toggle_unicode_borders()
            #self.ui.categories_list.add_text_color_rule('.*', py_cui.WHITE_ON_BLACK, 'contains', selected_color=py_cui.YELLOW_ON_BLACK)
            #self.ui.categories_list.add_key_command(py_cui.keys.KEY_ENTER, self.print_subs)

        def print_active_subs(self):
                self.active_category = self.ui.get_active_category()
                self.ui.sub_list.clear()
                for i in range(0, len(self.feed_parser.subs['category'][self.active_category])):
                        self.ui.sub_list.add_item(self.feed_parser.subs['category'][self.active_category][i]['title'])
        
        def print_feed_entries(self):
                title = self.ui.get_active_feed()
                title_list = []
                self.ui.post_list.clear()
                for i in range(0, len(self.feed_parser.subs['category'][self.active_category])):
                        if title == self.feed_parser.subs['category'][self.active_category][i]['title']:
                                self.feed_entries =  self.feed_parser.parse(self.feed_parser.subs['category'][self.active_category][i]['url']).entries
                                for entry in self.feed_entries:
                                        title_list.append(entry['title'])
                        self.ui.post_list.add_item_list(title_list)
        
        def print_feed_post(self):
                post_title = self.ui.get_active_post()
                
                post_content = ""
                for i in range(0, len(self.feed_entries)):
                        if self.feed_entries[i].title == post_title:
                                self.soup = BeautifulSoup(self.feed_entries[i]['description'], 'html.parser')
                                post_title = "Title: {}".format(self.feed_entries[i].title)
                                post_content = "Author: {}".format(self.feed_entries[i]['author'])
                                post_content = textwrap.shorten(post_title, width=40, placeholder='...') + '\n\n' + textwrap.dedent(post_content) + '\n\n' + textwrap.fill(self.soup.div.get_text(), width=75)
                                #post_content = self.feed_entries[i]['description']
                                #print(self.feed_entries[i].keys())
#                self.ui.post_content.clear()
                self.ui.post_content.set_title(post_content)
                #self.ui.post_content.set_title(self.config_parser.get_default_widget_fg_color("post_list"))
                self.ui.post_content.add_text_color_rule('Title: *', py_cui.BLUE_ON_BLACK, 'contains', match_type='line')
                self.ui.post_content.add_text_color_rule('Author: *', py_cui.MAGENTA_ON_BLACK, 'contains', match_type='line')
                self.ui.post_list.add_text_color_rule('.*',py_cui.WHITE_ON_BLACK ,'contains', match_type = 'line', selected_color = py_cui.YELLOW_ON_BLACK) 
                self.ui.post_list.set_color(py_cui.WHITE_ON_BLACK)
                 
        def open_in_browser(self): 
                for i in range(0, len(self.feed_entries)):
                        if self.feed_entries[i].title == self.ui.post_list.get():
                                webbrowser.open(self.feed_entries[i].link) 

        def main(self):
                self.ui_root.start()        
        
        #def set_colors(self):
