#!/usr/bin/env python3
import yaml
import feedparser

class ConfigParser:
        """ parses the config file """
        config_file = None
        def __init__(self, config):
                self.config = config
                with open(self.config) as file:
                        self.config_file = yaml.safe_load(file)
        def get_config_file(self):
                return self.config_file

        def get_default_widget_fg_color(self, widget):
                return self.config_file['default_colors'][widget]['fg'].upper()
        
        def get_default_widget_bg_color(self, widget):
                return self.config_file['default_colors'][widget]['bg'].upper()
        
        def get_selected_widget_fg_color(self, widget):
                return self.config_file['selected_colors'][widget]['fg'].upper()


        def get_selected_widget_bg_color(self, widget):
                return self.config_file['selected_colors'][widget]['bg'].upper()

class FeedParser:
        """ Parses subs in the feeds.yaml file """
        categories = []
        sub_titles = []
        sub_urls = []
        def __init__(self, subs_file):
                self.subs_file = subs_file
                with open(self.subs_file) as file:
                        self.subs = yaml.safe_load(file)
                for category in self.subs['category']:
                        self.categories.append(category)
                        for index in range(0, len(self.subs['category'][category])) :
                                self.sub_titles.append(self.subs['category'][category][index]['title'])
        
        def get_sub_urls(self):
                return self.sub_urls

        def get_feed_categories(self):
                return self.categories
        #def get_sub_titles(self, category)
        def parse(self, url):
                return feedparser.parse(url)
