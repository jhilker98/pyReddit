#!/usr/bin/env python3

import py_cui

class PyredditUI:
        """ Represents the UI of the PyReddit application."""
        def __init__(self, root):
                self.root = root
                self.categories_list = self.root.add_scroll_menu('Categories',0,0,row_span = 7, column_span = 2)
                self.sub_list = self.root.add_scroll_menu('Subreddit', 0, 2, row_span = 3, column_span = 2)
                self.post_list = self.root.add_scroll_menu('Posts', 3, 2, row_span = 4, column_span = 2)
                self.post_content = self.root.add_block_label('', 0, 4, row_span = 7, column_span = 3, center=False) 
                #self.post_content.toggle_border()

        def get_active_category(self):
                return self.categories_list.get()

        def get_active_feed(self):
                return self.sub_list.get()
   
        def get_active_post(self):
                return self.post_list.get()
        
        def set_color(self, fg_color, bg_color, widget):
                color_str = "py_cui.{}_on_{}".format(fg_color.upper(), bg_color.upper())
                self.widget.set_color(color_str)

#        def focus_categories(self):
#                self.root.move_focus(self.categories_list)
#
#        def focus_subreddits(self):
#                self.root.move_focus(self.sub_list)
